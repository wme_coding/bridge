package user;

import api.PostAPI;

public abstract class User {
    protected PostAPI postAPI;

    protected User(PostAPI postAPI) {
        this.postAPI = postAPI;
    }

    public void setPostAPI(PostAPI postAPI) {
        this.postAPI = postAPI;
    }

    public abstract void makePost();
}
