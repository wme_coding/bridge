package user;

import api.PostAPI;

public class UsualUser extends User{
    private String email;
    private String firstName;
    private String secondName;
    private String login;

    public UsualUser(PostAPI postAPI, String email, String firstName, String secondName, String login) {
        super(postAPI);
        this.email = email;
        this.firstName = firstName;
        this.secondName = secondName;
        this.login = login;
    }

    public void makePost() {
        postAPI.makePost(email);
    }
}
