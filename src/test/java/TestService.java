import api.FacebookPost;
import api.Instagram;
import api.TweeterPost;
import org.junit.Test;
import user.User;
import user.UsualUser;

public class TestService {

    @Test
    public void testService(){
        User user1 = new UsualUser(new FacebookPost(),
                "full.house@gmail.com", "Full", "House", "FH");
        User user2 = new UsualUser(new Instagram(),
                "anton.morozov@gmail.com", "Anton", "Morozov", "Yoha");

        user1.makePost();
        user2.makePost();

        user1.setPostAPI(new TweeterPost());
        user1.makePost();
    }
}
